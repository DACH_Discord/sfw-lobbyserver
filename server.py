from flask import Flask
from flask import request
from flask import jsonify
import time


app = Flask(__name__)
servers = []
responseTimes = {}

@app.route('/get_servers')
def get_servers():
    for i in range(0, len(servers)):
        if responseTimes[servers[i]] + 5 < time.time():
            servers.pop(i)


    return jsonify(servers)

@app.route('/add_server')
def add_server():
    if request.remote_addr not in servers:
        servers.append(request.remote_addr)
    responseTimes[request.remote_addr] = time.time()

    return jsonify({'added ip': request.remote_addr,
                    'at time': time.time()}), 200


@app.route('/del_server')
def del_server():
    for i in range(0, len(servers)):
        if servers[i] == request.remote_addr:
            servers.pop(i)

    return jsonify({'tried to delete': request.remote_addr}), 200
